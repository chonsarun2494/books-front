import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials"
export default NextAuth({
    providers: [
        CredentialsProvider({
          name: 'Credentials',
          credentials: {
            username: { label: "Username", type: "text", placeholder: "jsmith" },
            password: { label: "Password", type: "password" }
          },
          async authorize(credentials, req) {
            const res = await fetch("http://api:8328/login", {
              method: 'POST',
              body: JSON.stringify(credentials),
              headers: { "Content-Type": "application/json" }
            })

            const user = await res.json()
            //console.log("data is ", user.token)
            if (res.ok && user) {
              return user
            }
            return null
          }
        })
      ],
      session: {
        strategy: "jwt",
        maxAge: 30 * 24 * 60 * 60, // 30 days
      },
      callbacks: {
         async jwt ({ token, user }) {
          return { ...token, ...user}
        },
        async session({session, token, user}){
          session.user = token
          return session;
        }
        
    }
    
      
})
