// file: pages/book.js
import { useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';

export default function Book() {
    const [books, setBooks] = useState([]);
    const { data: session } = useSession();
    useEffect(() => {
        if (session) {
            fetchBooks(session.accessToken);
        }
        
    }, [session]);

    const fetchBooks = async (accessToken) => {
        try {
            console.log(accessToken)
            const response = await fetch('http://api:8328/book', {
                headers: {
                    'Authorization': `Bearer ${session.user.token}`
                }
                
            });
            if (response.ok) {
                const data = await response.json();
                setBooks(data);
            } else {
                console.error('Failed to fetch books:', response.statusText);
            }
        } catch (error) {
            console.error("Error fetching books:", error);
        }
    };

    const router = useRouter();

    const handleEditBook = (id) => {
        
        router.push(`/edit-book/${id}`);
    };

    const handleDeleteBook = (id) => {
        
        router.push(`/delete-book/${id}`);
    };



    return (
        <div className="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="overflow-x-auto">
                    <div className="align-middle inline-block min-w-full">
                        <div className="overflow-hidden">
                            <table className="min-w-full">
                                <thead>
                                    <tr>
                                        <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase">ID</th>
                                        <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase">Title</th>
                                        <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase">Author</th>
                                        <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase">Description</th>
                                        <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase">Price</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    {Array.isArray(books) && books.length > 0 ? (
                                        books.map((book) => (
                                            <tr key={book.id}>
                                                <td className="px-6 py-4">{book.id}</td>
                                                <td className="px-6 py-4">{book.title}</td>
                                                <td className="px-6 py-4">{book.author}</td>
                                                <td className="px-6 py-4">{book.description}</td>
                                                <td className="px-6 py-4">${book.price.toFixed(2)}</td>
                                                
                                            </tr>
                                        ))
                                    ) : (
                                        <tr>
                                            <td className="px-6 py-4 text-center" colSpan={6}>
                                                {Array.isArray(books) && books.length === 0
                                                    ? "No data"
                                                    : "Data is not in the correct format."}
                                            </td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ); 
}
