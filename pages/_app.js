import '@/styles/globals.css'
import Navbar from '@/components/Navbar'

import { SessionProvider } from "next-auth/react"

export default function App({
  Component, pageProps: { session, ...pageProps }
}) {
  return (
    <SessionProvider session={session}>
       <div className={"min-h-full "}>
        <Navbar />
        <Component {...pageProps}/>
      </div>
      
    </SessionProvider>
  )
}