// file: pages/register.js

import { useState } from 'react';
import { useRouter } from 'next/router';

export default function Register() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const router = useRouter();

    const handleRegister = async (event) => {
        event.preventDefault();
        
        try {
            const response = await fetch('http://api:8328//register', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ username, password })
            });

            if (response.ok) {
                // Redirect to the homepage after successful registration
                router.push('/');
            } else {
                // Handle errors (you may want to show some feedback to the user)
                console.error("Failed to register");
            }
        } catch (error) {
            console.error("Error registering user:", error);
            // Handle error (you may want to show some feedback to the user)
        }
    };

    return (
        <div className='min-h-screen bg-gray-100 flex items-center justify-center'>
            <form onSubmit={handleRegister} className='bg-white p-8 rounded shadow'>
                <div className='mb-4'>
                    <label htmlFor="username" className='block text-gray-700'>Username:</label>
                    <input
                        type="text"
                        id="username"
                        name="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        className='mt-1 p-2 w-full border rounded'
                    />
                </div>
                <div className='mb-4'>
                    <label htmlFor="password" className='block text-gray-700'>Password:</label>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className='mt-1 p-2 w-full border rounded'
                    />
                </div>
                <button type="submit" className='w-full p-2 bg-blue-500 text-white rounded'>Register</button>
            </form>
        </div>
    );
    
}
