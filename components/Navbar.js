import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSession, signIn, signOut } from "next-auth/react";

export default function Navbar() {
    const { data: session } = useSession();
    const router = useRouter();
    
    const handleSignIn = async () => {
        try {
            const response = await signIn();
            if (response) {
                // Redirect to the book page after successful sign in
                router.push('/book');
            }
        } catch (error) {
            console.error("Error during sign in:", error);
        }
    };

    return (
        <nav className="bg-black text-white p-4 flex flex-row gap-5 fixed left-0 right-0">
            <div>
                <Link href="/">
                    Home
                </Link>
            </div>
            <div>
                {session ? (
                    <>
                        {/* Signed in as {session.user.email} <br/> */}
                        <button onClick={() => signOut()}>Sign out</button>
                    </>
                ) : (
                    <button onClick={handleSignIn}>Sign in</button>
                )}
            </div>
            <div>
            {!session && (
                    <Link href="/register">ลงทะเบียนผู้ใช้งาน</Link>
                )}
            </div>
            <div>
                <Link href="/book">หนังสือ</Link>
            </div>
        </nav>
    );
}
